package it.venis.iris2.controller;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import it.venis.iris2.AppConfigDbIris;
import it.venis.iris2.AppConfigITB;
import it.venis.iris2.db.utils.CDbUtils;
import it.venis.iris2.elastic.utils.CElasticSUtils;
import it.venis.iris2.oggettidbiris.VfixEndpointNotifiche;
import it.venis.iris2.oggettidbiris.VfixLogMimuv;
import it.venis.iris2.oggettidbiris.VfixLogMimuvExample;
import it.venis.iris2.oggettidbiris.VfixLogMimuvMapper;
import it.venis.iris2.oggettidbiris.VfixSegnalazione_SIT;
import it.venis.iris2.oggettidbiris.VfixTmpMimuv;
import it.venis.iris2.oggettidbiris.VfixTmpMimuvExample;
import it.venis.iris2.oggettidbiris.VfixTmpMimuvMapper;
import it.venis.iris2.oggettidbiris.VfixUtenteRegistrato;
import it.venis.iris2.oggettidbiris.VfixUtenteRegistratoExample;
import it.venis.iris2.oggettidbiris.VfixUtenteRegistratoMapper;
import it.venis.iris2.oggettidbiris.dao.CustomMapper;
import it.venis.iris2.pushnotification.CWebPush;
import it.venis.iris2.security.CGestioneUtente;
import it.venis.iris2.utils.CCoopApplicativa;
import it.venis.iris2.utils.CUtils;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@RestController
public class ApiController {

	public static String bySpid = "SPID";
	public static String byDiMe = "DIME";
	public static String byOper = "OPER";
	public static String byFree = "FREE";
	public static String byLegacy = "LEGACY";
	
	
	public ApiController() {
		// TODO Auto-generated constructor stub
	}

	// --------------------------------------- aggiorna la segnalazione (notifica e update elasticsearch)
	
	@CrossOrigin (origins = "*" )
	@PostMapping
    @RequestMapping(value = "/api/aggiornasegnalazione",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public boolean aggiornasegnalazione(@RequestParam(value="id_segnalazione", defaultValue="0") Long IdSegn,
    												   @RequestParam(value="stato", defaultValue="0") byte bStato,
    												   @RequestParam(value="payload", defaultValue="") String payload)
    {
		try {
			if (IdSegn == 0) return false;
			CDbUtils dbu = new CDbUtils();
			dbu.componilistasegnalazioni(IdSegn.toString(), true);
			dbu.modificaITB(IdSegn, false);
			
			// notifica ad un referente esterno (es. Mimuv)
			try {
				if (bStato == CUtils.N_OP_TRASFERIMENTO)
					dbu.esportaSegnalazione (IdSegn, bStato, false);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String sStato = "";
			switch (bStato)
			{
				case CUtils.N_OP_CHIUSA: {sStato = "Chiusa"; break;} 
				case CUtils.N_OP_IN_CARICO: {sStato = "In carico"; break;}
				case CUtils.N_OP_INOLTRATA: {sStato = "Inoltrata"; break;}
				case CUtils.N_OP_RESPINTA: {sStato = "Respinta"; break;}
				case CUtils.N_OP_RISOLTA: {sStato = "Risolta"; break;}
				case CUtils.N_OP_SOSPESA: {sStato = "Sospesa"; break;}
				case CUtils.N_OP_TRASFERIMENTO: {sStato = "Trasferita"; break;}
				default: sStato = " - ";
			}

			if (payload.isEmpty()) payload = "{\"body\":\"Aggiornamento della segnalazione " + IdSegn + ": " + sStato + "\", \"idsegnalazione\": " + IdSegn + "}";
			else return false;
			
			if (bStato == CUtils.N_OP_CHIUSA || bStato == CUtils.N_OP_IN_CARICO  || bStato == CUtils.N_OP_RISOLTA)
			{
				Long lIdUser = dbu.getIdSegnalatore(IdSegn);
				CWebPush wp = new CWebPush();		
				List<VfixEndpointNotifiche> le = dbu.selectEndpoint(lIdUser.toString());  // poi potrò indicare lo user
				
				for (VfixEndpointNotifiche theEp : le) {			
					wp.doIt(theEp.getEndpoint(), payload);		
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;
    }

	
	// attenzione: questa funzione deve funzionare con un bearer (se c'è) oppure con un CF
	@CrossOrigin
	@GetMapping
    @RequestMapping(value = "/api/listaSegnalazioniIris",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public String listaSegnalazioniIris(HttpServletRequest request,
    							   @RequestParam(value="id_segnalazione", defaultValue="0") Long lIdSegnalazione,
			  					   @RequestParam(value="q", defaultValue="") String sQuery,
			  					   @RequestParam(value="from", defaultValue="0") int wFrom,
			  					   @RequestParam(value="size", defaultValue="10") int wSize,
			  					   @RequestParam(value="sort", defaultValue="") String sSort,
			  					   @RequestParam(value="filtro", defaultValue="") String sFiltro,
			  					   @RequestParam(value="dtinizio", defaultValue="") String dtInizio,
			  					   @RequestParam(value="dtfine", defaultValue="")  String dtFine,
			  					   @RequestParam(value="theBearer", defaultValue="")  String theBearer,
			  					   @RequestParam(value="theCF", defaultValue="")  String theCF)
    {
		
		VfixUtenteRegistrato ut = null;
		CDbUtils dut = new CDbUtils();
		String sIdUtente = null;

		// tenta con il CF
		if (!theCF.equals(""))
		{
			ut = dut.selectUtenteRegistratoByCF(theCF);
			if (ut != null) sIdUtente = ut.getIdUtenteRegistrato().toString();
		}
		else
		{
			// tenta con il bearer come parametro
			if (!theBearer.equals(""))
			{
				ut = dut.selectUtenteRegistratoByBearer(theBearer);
				if (ut != null)	sIdUtente = ut.getIdUtenteRegistrato().toString();
			}
			else
			{
				// tenta poi con il bearer nell'authorization
				// però in tal caso l'authentication sovrascriverebbe la basic authentication e ci ritroveremmo nel caso searchmine, pertanto non va trattata
			}
		}

		
		CElasticSUtils esU = new CElasticSUtils();
		if (sIdUtente == null) return "{}";
		String sJson = esU.bCerca(lIdSegnalazione, sQuery, wFrom, wSize, sSort, sFiltro, dtInizio, dtFine, sIdUtente);
        return sJson;     
    }

	
	// ottiene le informazioni sull'utente segnalatore e le restituisce al backoffice

	@CrossOrigin
	@GetMapping
    @RequestMapping(value = "/api/getInfoSegnalatore",  method={RequestMethod.GET, RequestMethod.POST})
    public String getInfoSegnalatore(HttpServletRequest request,
    							   @RequestParam(value="id_segnalatore", defaultValue="0") Long lIdSegnalatore,
    							   @RequestParam(value="id_segnalazione", defaultValue="0") Long lIdSegnalazione)
    {
		
		VfixUtenteRegistrato ut = null;
		String sDatiUtente = "";
		String sSecret = "";
		String sTipoIdentity = "";
		
		if (lIdSegnalazione != 0)
			ut = new CDbUtils().selectUtenteRegistratoByIdSegn(lIdSegnalazione);
		else
			ut = new CDbUtils().selectUtenteRegistratoById(lIdSegnalatore);
		
		// servirebbe fare un MD5...
		if (ut.getBearer() == null || ut.getBearer().equals(""))
		{
			// in questo caso, ci troviamo tipicamente nella situazione classica... vengono recuperati i dati dalle colonne...
			sTipoIdentity = byLegacy; 
			sDatiUtente = ut.getCognome() + "|" + ut.getNome() + "|" + ut.getEmail() + "|" + ut.getTelefono() + "|" + sTipoIdentity;
		}
		else
		{
			CGestioneUtente gu = new CGestioneUtente();
			if (gu.sGetDatiByUncredited(ut.getBearer()))
			{
				sTipoIdentity = byFree;
				sDatiUtente = gu.getsCognome() + "|" + gu.getsNome()  + "|" + gu.getsEMail() + "|" +  gu.getsTelefono() + "|" + sTipoIdentity;
			}
			else
				if (gu.sGetDatiByDiMe(ut.getBearer()))
				{
					sTipoIdentity = byDiMe;
					sDatiUtente = gu.getsCognome() + "|" + gu.getsNome()  + "|" + gu.getsEMail() + "|" +  gu.getsTelefono() + "|" + sTipoIdentity;
				}
				else
					if (gu.sGetDatiBySpid(ut.getBearer()))
					{
						sTipoIdentity = bySpid;
						sDatiUtente = gu.getsCognome() + "|" + gu.getsNome()  + "|" + gu.getsEMail() + "|" +  gu.getsTelefono() + "|" + sTipoIdentity;
					}
					else
					{
						// operatore che si è collegato
						sTipoIdentity = byOper;
						sDatiUtente = ut.getToken() + "| | | |" + sTipoIdentity;
					}
		}
		
		byte[] bToken1 = org.bouncycastle.util.encoders.Base64.encode(sDatiUtente.getBytes());
		sSecret = new String(bToken1);
		String sJson = sSecret;
        return sJson;
    }

	
	@CrossOrigin (origins = "*" )
	@PostMapping
    @RequestMapping(value = "/api/elasticsearchupdate",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public String elasticsearchupdate(HttpServletRequest request, @RequestBody (required = false) String body,
    								  @RequestParam(value="id_segnalazione", defaultValue="0") Long lIdSegnalazione)
    {	
		// TODO va indicato all'operatore l'aggiunta di un commento
		// TODO anche al cittadino interessato, no?
		try {
			CDbUtils dbu = new CDbUtils();
			if (lIdSegnalazione == 0) return null;
			return dbu.componilistasegnalazioni(lIdSegnalazione.toString(), true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		return null;
    }	
	

	@CrossOrigin (origins = "*" )
    @RequestMapping(value = "/api/exportsegn",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public String exportSegn(HttpServletRequest request, @RequestParam(value="theSegn", required=false) Long theSegn, 
    													 @RequestParam(value="nTipoOp", required=false) byte nTipoOp,    		
    													 @RequestBody (required = false) String body)
    {
		CDbUtils dbu = new CDbUtils();		
		if (theSegn == null && body != null) 
		{
			JsonObject jsonObj = (JsonObject) new JsonParser().parse(body);
			theSegn = jsonObj.get("theSegn").getAsLong();
		}
		if (theSegn != null)
		{
			dbu.esportaSegnalazione(theSegn, nTipoOp, false);
		}
		return "";
    }

	@CrossOrigin (origins = "*" )
	@PostMapping
    @RequestMapping(value = "/api/inserisciext",  produces={"application/json"},  method={RequestMethod.POST})
    public String inserisciExt(HttpServletRequest request, @RequestBody (required = false) String body,
    							   @RequestParam(value="datiSegnalazione", defaultValue="") String datiSegnalazione)
    {	
		CDbUtils dbu = new CDbUtils();
    	if (datiSegnalazione.equals("") && body != null) datiSegnalazione = body.toString();
		return dbu.InserisciDatisuDB(datiSegnalazione, null, true);     
    }
	
	
	// funzione per RICEVERE gli aggiornamenti di una segnalazione dall'esterno (cooperatore)
	@CrossOrigin (origins = "*" )
	@PostMapping
    @RequestMapping(value = "/api/coopaggiornasegnalazione",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public String coopaggiornasegnalazione(@RequestParam(value="datiCooperatore", defaultValue="") String datiCoop, @RequestBody (required = false) String body)
    {
		if (datiCoop.equals("") && body != null) datiCoop = body.toString();
		try {
			CCoopApplicativa coop = new CCoopApplicativa();
			return coop.sCoopAggiorna(datiCoop);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    }
	
	
	
	@CrossOrigin (origins = "*" )
	@PostMapping
    @RequestMapping(value = "/api/aggiornamentoGenerico",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public boolean aggiornamentoGenerico()
    {
		try {
			AnnotationConfigApplicationContext ctx;
			ctx = new AnnotationConfigApplicationContext();
			ctx.register(AppConfigDbIris.class);
			ctx.refresh();
			CDbUtils dbu = new CDbUtils();
			
			CustomMapper cm = ctx.getBean(CustomMapper.class);

			List<Map<String, String>> listaSegn = null;
			listaSegn = cm.getListaSegnalazioniDaAggiornare();
			
			dbu.componilistasegnalazioniGruppo(listaSegn, true);
			dbu.componilistamarkerGenerico(listaSegn);
			
			ctx.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;
    }
	
	
	// --------------------------------------- ELIMINAZIONE DI UNA SEGNALAZIONE (duplicata il 10.06.19 perché chiamata anche da IrisAdmin) 
	
	@CrossOrigin (origins = "*" )
	@PostMapping
    @RequestMapping(value = "/api/rimuovisegnalazione",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public String rimuovisegnalazione(HttpServletRequest request, @RequestBody (required = false) String body,
    							   @RequestParam(value="datiInopportuna", defaultValue="") String datiInopportuna)
    {	
		CDbUtils dbu = new CDbUtils();
		if (datiInopportuna.equals("") && body != null) datiInopportuna = body.toString();		
		return dbu.EliminaSegnalazioneLogica(datiInopportuna);     
    }
	
}
