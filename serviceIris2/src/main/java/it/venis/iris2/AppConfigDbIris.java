package it.venis.iris2;

import java.util.Properties;

import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
//@EnableTransactionManagement
//@Import({AppConfigITB.class})
public class AppConfigDbIris {

	// il primo DataSource è marcato come @Primary per essere gestito dalla autoconfiguration di Spring...
	@Bean (name = "dbIris")
	@Primary	
	 public  DataSource dsIris() {
 		BasicDataSource dataSource = new BasicDataSource();
 		
 	   	java.util.ResourceBundle mybundle=java.util.ResourceBundle.getBundle("application");
    	String ambiente = mybundle.getString("ambiente");
    	if (ambiente.equalsIgnoreCase("TEST"))
    	{
            dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
            dataSource.setUrl("jdbc:oracle:thin:@SERVER:PORT/SERVICE");   		
    	}
    	else
    	{
            dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
            dataSource.setUrl("jdbc:oracle:thin:@SERVER:PORT/SERVICE");   		
    	}
        dataSource.setUsername("USER");
        dataSource.setPassword("PWD");
       return dataSource;
   }

   
   @Bean (name = "sqlSessionFactoryIris")
   @Primary
   public SqlSessionFactory sqlSessionFactory() throws Exception {
      SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
      sessionFactory.setDataSource(dsIris());
      
      return sessionFactory.getObject();
   }


   @Bean
   public MapperScannerConfigurer mapperScannerConfigurer4() {
       MapperScannerConfigurer configurer = new MapperScannerConfigurer();
       configurer.setBasePackage("it.venis.iris2.oggettidbiris");
       configurer.setSqlSessionFactoryBeanName("sqlSessionFactoryIris");
       return configurer;
   }
   
   /*@Bean(name = "transactionManager")
   public DataSourceTransactionManager transactionManager() {
       return new DataSourceTransactionManager(dsIris());
   }
  
   @Bean(name = "transactionManager")
   public PlatformTransactionManager txManager() {
       return new DataSourceTransactionManager(dsIris());
   }
   
   public PlatformTransactionManager annotationDrivenTransactionManager() {
       return txManager();
   }*/
   
   
   // mail sender
   @Bean
	public JavaMailSender javaMailSender() {
		 java.util.ResourceBundle mybundle=java.util.ResourceBundle.getBundle("application");
	     JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
	     Properties mailProperties = new Properties();
	     mailProperties.put("mail.smtp.auth", mybundle.getString("spring.mail.properties.mail.smtp.auth"));
	     mailSender.setJavaMailProperties(mailProperties);
	     mailSender.setHost(mybundle.getString("spring.mail.host"));
	     return mailSender;
	}
   
}

