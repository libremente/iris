package it.venis.iris2.oggettidbiris;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VfixSegnalazioneExample {
    /**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table VFIX_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	protected String orderByClause;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table VFIX_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	protected boolean distinct;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table VFIX_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	protected List<Criteria> oredCriteria;

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public VfixSegnalazioneExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table VFIX_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value,
				String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property
						+ " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1,
				Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property
						+ " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdSegnalazioneIsNull() {
			addCriterion("ID_SEGNALAZIONE is null");
			return (Criteria) this;
		}

		public Criteria andIdSegnalazioneIsNotNull() {
			addCriterion("ID_SEGNALAZIONE is not null");
			return (Criteria) this;
		}

		public Criteria andIdSegnalazioneEqualTo(Long value) {
			addCriterion("ID_SEGNALAZIONE =", value, "idSegnalazione");
			return (Criteria) this;
		}

		public Criteria andIdSegnalazioneNotEqualTo(Long value) {
			addCriterion("ID_SEGNALAZIONE <>", value, "idSegnalazione");
			return (Criteria) this;
		}

		public Criteria andIdSegnalazioneGreaterThan(Long value) {
			addCriterion("ID_SEGNALAZIONE >", value, "idSegnalazione");
			return (Criteria) this;
		}

		public Criteria andIdSegnalazioneGreaterThanOrEqualTo(Long value) {
			addCriterion("ID_SEGNALAZIONE >=", value, "idSegnalazione");
			return (Criteria) this;
		}

		public Criteria andIdSegnalazioneLessThan(Long value) {
			addCriterion("ID_SEGNALAZIONE <", value, "idSegnalazione");
			return (Criteria) this;
		}

		public Criteria andIdSegnalazioneLessThanOrEqualTo(Long value) {
			addCriterion("ID_SEGNALAZIONE <=", value, "idSegnalazione");
			return (Criteria) this;
		}

		public Criteria andIdSegnalazioneIn(List<Long> values) {
			addCriterion("ID_SEGNALAZIONE in", values, "idSegnalazione");
			return (Criteria) this;
		}

		public Criteria andIdSegnalazioneNotIn(List<Long> values) {
			addCriterion("ID_SEGNALAZIONE not in", values, "idSegnalazione");
			return (Criteria) this;
		}

		public Criteria andIdSegnalazioneBetween(Long value1, Long value2) {
			addCriterion("ID_SEGNALAZIONE between", value1, value2,
					"idSegnalazione");
			return (Criteria) this;
		}

		public Criteria andIdSegnalazioneNotBetween(Long value1, Long value2) {
			addCriterion("ID_SEGNALAZIONE not between", value1, value2,
					"idSegnalazione");
			return (Criteria) this;
		}

		public Criteria andDataSegnalazioneIsNull() {
			addCriterion("DATA_SEGNALAZIONE is null");
			return (Criteria) this;
		}

		public Criteria andDataSegnalazioneIsNotNull() {
			addCriterion("DATA_SEGNALAZIONE is not null");
			return (Criteria) this;
		}

		public Criteria andDataSegnalazioneEqualTo(Date value) {
			addCriterion("DATA_SEGNALAZIONE =", value, "dataSegnalazione");
			return (Criteria) this;
		}

		public Criteria andDataSegnalazioneNotEqualTo(Date value) {
			addCriterion("DATA_SEGNALAZIONE <>", value, "dataSegnalazione");
			return (Criteria) this;
		}

		public Criteria andDataSegnalazioneGreaterThan(Date value) {
			addCriterion("DATA_SEGNALAZIONE >", value, "dataSegnalazione");
			return (Criteria) this;
		}

		public Criteria andDataSegnalazioneGreaterThanOrEqualTo(Date value) {
			addCriterion("DATA_SEGNALAZIONE >=", value, "dataSegnalazione");
			return (Criteria) this;
		}

		public Criteria andDataSegnalazioneLessThan(Date value) {
			addCriterion("DATA_SEGNALAZIONE <", value, "dataSegnalazione");
			return (Criteria) this;
		}

		public Criteria andDataSegnalazioneLessThanOrEqualTo(Date value) {
			addCriterion("DATA_SEGNALAZIONE <=", value, "dataSegnalazione");
			return (Criteria) this;
		}

		public Criteria andDataSegnalazioneIn(List<Date> values) {
			addCriterion("DATA_SEGNALAZIONE in", values, "dataSegnalazione");
			return (Criteria) this;
		}

		public Criteria andDataSegnalazioneNotIn(List<Date> values) {
			addCriterion("DATA_SEGNALAZIONE not in", values, "dataSegnalazione");
			return (Criteria) this;
		}

		public Criteria andDataSegnalazioneBetween(Date value1, Date value2) {
			addCriterion("DATA_SEGNALAZIONE between", value1, value2,
					"dataSegnalazione");
			return (Criteria) this;
		}

		public Criteria andDataSegnalazioneNotBetween(Date value1, Date value2) {
			addCriterion("DATA_SEGNALAZIONE not between", value1, value2,
					"dataSegnalazione");
			return (Criteria) this;
		}

		public Criteria andPtrUtenteIsNull() {
			addCriterion("PTR_UTENTE is null");
			return (Criteria) this;
		}

		public Criteria andPtrUtenteIsNotNull() {
			addCriterion("PTR_UTENTE is not null");
			return (Criteria) this;
		}

		public Criteria andPtrUtenteEqualTo(Long value) {
			addCriterion("PTR_UTENTE =", value, "ptrUtente");
			return (Criteria) this;
		}

		public Criteria andPtrUtenteNotEqualTo(Long value) {
			addCriterion("PTR_UTENTE <>", value, "ptrUtente");
			return (Criteria) this;
		}

		public Criteria andPtrUtenteGreaterThan(Long value) {
			addCriterion("PTR_UTENTE >", value, "ptrUtente");
			return (Criteria) this;
		}

		public Criteria andPtrUtenteGreaterThanOrEqualTo(Long value) {
			addCriterion("PTR_UTENTE >=", value, "ptrUtente");
			return (Criteria) this;
		}

		public Criteria andPtrUtenteLessThan(Long value) {
			addCriterion("PTR_UTENTE <", value, "ptrUtente");
			return (Criteria) this;
		}

		public Criteria andPtrUtenteLessThanOrEqualTo(Long value) {
			addCriterion("PTR_UTENTE <=", value, "ptrUtente");
			return (Criteria) this;
		}

		public Criteria andPtrUtenteIn(List<Long> values) {
			addCriterion("PTR_UTENTE in", values, "ptrUtente");
			return (Criteria) this;
		}

		public Criteria andPtrUtenteNotIn(List<Long> values) {
			addCriterion("PTR_UTENTE not in", values, "ptrUtente");
			return (Criteria) this;
		}

		public Criteria andPtrUtenteBetween(Long value1, Long value2) {
			addCriterion("PTR_UTENTE between", value1, value2, "ptrUtente");
			return (Criteria) this;
		}

		public Criteria andPtrUtenteNotBetween(Long value1, Long value2) {
			addCriterion("PTR_UTENTE not between", value1, value2, "ptrUtente");
			return (Criteria) this;
		}

		public Criteria andSubjectIsNull() {
			addCriterion("SUBJECT is null");
			return (Criteria) this;
		}

		public Criteria andSubjectIsNotNull() {
			addCriterion("SUBJECT is not null");
			return (Criteria) this;
		}

		public Criteria andSubjectEqualTo(String value) {
			addCriterion("SUBJECT =", value, "subject");
			return (Criteria) this;
		}

		public Criteria andSubjectNotEqualTo(String value) {
			addCriterion("SUBJECT <>", value, "subject");
			return (Criteria) this;
		}

		public Criteria andSubjectGreaterThan(String value) {
			addCriterion("SUBJECT >", value, "subject");
			return (Criteria) this;
		}

		public Criteria andSubjectGreaterThanOrEqualTo(String value) {
			addCriterion("SUBJECT >=", value, "subject");
			return (Criteria) this;
		}

		public Criteria andSubjectLessThan(String value) {
			addCriterion("SUBJECT <", value, "subject");
			return (Criteria) this;
		}

		public Criteria andSubjectLessThanOrEqualTo(String value) {
			addCriterion("SUBJECT <=", value, "subject");
			return (Criteria) this;
		}

		public Criteria andSubjectLike(String value) {
			addCriterion("SUBJECT like", value, "subject");
			return (Criteria) this;
		}

		public Criteria andSubjectNotLike(String value) {
			addCriterion("SUBJECT not like", value, "subject");
			return (Criteria) this;
		}

		public Criteria andSubjectIn(List<String> values) {
			addCriterion("SUBJECT in", values, "subject");
			return (Criteria) this;
		}

		public Criteria andSubjectNotIn(List<String> values) {
			addCriterion("SUBJECT not in", values, "subject");
			return (Criteria) this;
		}

		public Criteria andSubjectBetween(String value1, String value2) {
			addCriterion("SUBJECT between", value1, value2, "subject");
			return (Criteria) this;
		}

		public Criteria andSubjectNotBetween(String value1, String value2) {
			addCriterion("SUBJECT not between", value1, value2, "subject");
			return (Criteria) this;
		}

		public Criteria andNoteIsNull() {
			addCriterion("NOTE is null");
			return (Criteria) this;
		}

		public Criteria andNoteIsNotNull() {
			addCriterion("NOTE is not null");
			return (Criteria) this;
		}

		public Criteria andNoteEqualTo(String value) {
			addCriterion("NOTE =", value, "note");
			return (Criteria) this;
		}

		public Criteria andNoteNotEqualTo(String value) {
			addCriterion("NOTE <>", value, "note");
			return (Criteria) this;
		}

		public Criteria andNoteGreaterThan(String value) {
			addCriterion("NOTE >", value, "note");
			return (Criteria) this;
		}

		public Criteria andNoteGreaterThanOrEqualTo(String value) {
			addCriterion("NOTE >=", value, "note");
			return (Criteria) this;
		}

		public Criteria andNoteLessThan(String value) {
			addCriterion("NOTE <", value, "note");
			return (Criteria) this;
		}

		public Criteria andNoteLessThanOrEqualTo(String value) {
			addCriterion("NOTE <=", value, "note");
			return (Criteria) this;
		}

		public Criteria andNoteLike(String value) {
			addCriterion("NOTE like", value, "note");
			return (Criteria) this;
		}

		public Criteria andNoteNotLike(String value) {
			addCriterion("NOTE not like", value, "note");
			return (Criteria) this;
		}

		public Criteria andNoteIn(List<String> values) {
			addCriterion("NOTE in", values, "note");
			return (Criteria) this;
		}

		public Criteria andNoteNotIn(List<String> values) {
			addCriterion("NOTE not in", values, "note");
			return (Criteria) this;
		}

		public Criteria andNoteBetween(String value1, String value2) {
			addCriterion("NOTE between", value1, value2, "note");
			return (Criteria) this;
		}

		public Criteria andNoteNotBetween(String value1, String value2) {
			addCriterion("NOTE not between", value1, value2, "note");
			return (Criteria) this;
		}

		public Criteria andIndicazioniIsNull() {
			addCriterion("INDICAZIONI is null");
			return (Criteria) this;
		}

		public Criteria andIndicazioniIsNotNull() {
			addCriterion("INDICAZIONI is not null");
			return (Criteria) this;
		}

		public Criteria andIndicazioniEqualTo(String value) {
			addCriterion("INDICAZIONI =", value, "indicazioni");
			return (Criteria) this;
		}

		public Criteria andIndicazioniNotEqualTo(String value) {
			addCriterion("INDICAZIONI <>", value, "indicazioni");
			return (Criteria) this;
		}

		public Criteria andIndicazioniGreaterThan(String value) {
			addCriterion("INDICAZIONI >", value, "indicazioni");
			return (Criteria) this;
		}

		public Criteria andIndicazioniGreaterThanOrEqualTo(String value) {
			addCriterion("INDICAZIONI >=", value, "indicazioni");
			return (Criteria) this;
		}

		public Criteria andIndicazioniLessThan(String value) {
			addCriterion("INDICAZIONI <", value, "indicazioni");
			return (Criteria) this;
		}

		public Criteria andIndicazioniLessThanOrEqualTo(String value) {
			addCriterion("INDICAZIONI <=", value, "indicazioni");
			return (Criteria) this;
		}

		public Criteria andIndicazioniLike(String value) {
			addCriterion("INDICAZIONI like", value, "indicazioni");
			return (Criteria) this;
		}

		public Criteria andIndicazioniNotLike(String value) {
			addCriterion("INDICAZIONI not like", value, "indicazioni");
			return (Criteria) this;
		}

		public Criteria andIndicazioniIn(List<String> values) {
			addCriterion("INDICAZIONI in", values, "indicazioni");
			return (Criteria) this;
		}

		public Criteria andIndicazioniNotIn(List<String> values) {
			addCriterion("INDICAZIONI not in", values, "indicazioni");
			return (Criteria) this;
		}

		public Criteria andIndicazioniBetween(String value1, String value2) {
			addCriterion("INDICAZIONI between", value1, value2, "indicazioni");
			return (Criteria) this;
		}

		public Criteria andIndicazioniNotBetween(String value1, String value2) {
			addCriterion("INDICAZIONI not between", value1, value2,
					"indicazioni");
			return (Criteria) this;
		}

		public Criteria andFkTipologiaIsNull() {
			addCriterion("FK_TIPOLOGIA is null");
			return (Criteria) this;
		}

		public Criteria andFkTipologiaIsNotNull() {
			addCriterion("FK_TIPOLOGIA is not null");
			return (Criteria) this;
		}

		public Criteria andFkTipologiaEqualTo(Integer value) {
			addCriterion("FK_TIPOLOGIA =", value, "fkTipologia");
			return (Criteria) this;
		}

		public Criteria andFkTipologiaNotEqualTo(Integer value) {
			addCriterion("FK_TIPOLOGIA <>", value, "fkTipologia");
			return (Criteria) this;
		}

		public Criteria andFkTipologiaGreaterThan(Integer value) {
			addCriterion("FK_TIPOLOGIA >", value, "fkTipologia");
			return (Criteria) this;
		}

		public Criteria andFkTipologiaGreaterThanOrEqualTo(Integer value) {
			addCriterion("FK_TIPOLOGIA >=", value, "fkTipologia");
			return (Criteria) this;
		}

		public Criteria andFkTipologiaLessThan(Integer value) {
			addCriterion("FK_TIPOLOGIA <", value, "fkTipologia");
			return (Criteria) this;
		}

		public Criteria andFkTipologiaLessThanOrEqualTo(Integer value) {
			addCriterion("FK_TIPOLOGIA <=", value, "fkTipologia");
			return (Criteria) this;
		}

		public Criteria andFkTipologiaIn(List<Integer> values) {
			addCriterion("FK_TIPOLOGIA in", values, "fkTipologia");
			return (Criteria) this;
		}

		public Criteria andFkTipologiaNotIn(List<Integer> values) {
			addCriterion("FK_TIPOLOGIA not in", values, "fkTipologia");
			return (Criteria) this;
		}

		public Criteria andFkTipologiaBetween(Integer value1, Integer value2) {
			addCriterion("FK_TIPOLOGIA between", value1, value2, "fkTipologia");
			return (Criteria) this;
		}

		public Criteria andFkTipologiaNotBetween(Integer value1, Integer value2) {
			addCriterion("FK_TIPOLOGIA not between", value1, value2,
					"fkTipologia");
			return (Criteria) this;
		}

		public Criteria andLogidIsNull() {
			addCriterion("LOGID is null");
			return (Criteria) this;
		}

		public Criteria andLogidIsNotNull() {
			addCriterion("LOGID is not null");
			return (Criteria) this;
		}

		public Criteria andLogidEqualTo(BigDecimal value) {
			addCriterion("LOGID =", value, "logid");
			return (Criteria) this;
		}

		public Criteria andLogidNotEqualTo(BigDecimal value) {
			addCriterion("LOGID <>", value, "logid");
			return (Criteria) this;
		}

		public Criteria andLogidGreaterThan(BigDecimal value) {
			addCriterion("LOGID >", value, "logid");
			return (Criteria) this;
		}

		public Criteria andLogidGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("LOGID >=", value, "logid");
			return (Criteria) this;
		}

		public Criteria andLogidLessThan(BigDecimal value) {
			addCriterion("LOGID <", value, "logid");
			return (Criteria) this;
		}

		public Criteria andLogidLessThanOrEqualTo(BigDecimal value) {
			addCriterion("LOGID <=", value, "logid");
			return (Criteria) this;
		}

		public Criteria andLogidIn(List<BigDecimal> values) {
			addCriterion("LOGID in", values, "logid");
			return (Criteria) this;
		}

		public Criteria andLogidNotIn(List<BigDecimal> values) {
			addCriterion("LOGID not in", values, "logid");
			return (Criteria) this;
		}

		public Criteria andLogidBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("LOGID between", value1, value2, "logid");
			return (Criteria) this;
		}

		public Criteria andLogidNotBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("LOGID not between", value1, value2, "logid");
			return (Criteria) this;
		}

		public Criteria andLogdataIsNull() {
			addCriterion("LOGDATA is null");
			return (Criteria) this;
		}

		public Criteria andLogdataIsNotNull() {
			addCriterion("LOGDATA is not null");
			return (Criteria) this;
		}

		public Criteria andLogdataEqualTo(Date value) {
			addCriterion("LOGDATA =", value, "logdata");
			return (Criteria) this;
		}

		public Criteria andLogdataNotEqualTo(Date value) {
			addCriterion("LOGDATA <>", value, "logdata");
			return (Criteria) this;
		}

		public Criteria andLogdataGreaterThan(Date value) {
			addCriterion("LOGDATA >", value, "logdata");
			return (Criteria) this;
		}

		public Criteria andLogdataGreaterThanOrEqualTo(Date value) {
			addCriterion("LOGDATA >=", value, "logdata");
			return (Criteria) this;
		}

		public Criteria andLogdataLessThan(Date value) {
			addCriterion("LOGDATA <", value, "logdata");
			return (Criteria) this;
		}

		public Criteria andLogdataLessThanOrEqualTo(Date value) {
			addCriterion("LOGDATA <=", value, "logdata");
			return (Criteria) this;
		}

		public Criteria andLogdataIn(List<Date> values) {
			addCriterion("LOGDATA in", values, "logdata");
			return (Criteria) this;
		}

		public Criteria andLogdataNotIn(List<Date> values) {
			addCriterion("LOGDATA not in", values, "logdata");
			return (Criteria) this;
		}

		public Criteria andLogdataBetween(Date value1, Date value2) {
			addCriterion("LOGDATA between", value1, value2, "logdata");
			return (Criteria) this;
		}

		public Criteria andLogdataNotBetween(Date value1, Date value2) {
			addCriterion("LOGDATA not between", value1, value2, "logdata");
			return (Criteria) this;
		}

		public Criteria andLoguserIsNull() {
			addCriterion("LOGUSER is null");
			return (Criteria) this;
		}

		public Criteria andLoguserIsNotNull() {
			addCriterion("LOGUSER is not null");
			return (Criteria) this;
		}

		public Criteria andLoguserEqualTo(String value) {
			addCriterion("LOGUSER =", value, "loguser");
			return (Criteria) this;
		}

		public Criteria andLoguserNotEqualTo(String value) {
			addCriterion("LOGUSER <>", value, "loguser");
			return (Criteria) this;
		}

		public Criteria andLoguserGreaterThan(String value) {
			addCriterion("LOGUSER >", value, "loguser");
			return (Criteria) this;
		}

		public Criteria andLoguserGreaterThanOrEqualTo(String value) {
			addCriterion("LOGUSER >=", value, "loguser");
			return (Criteria) this;
		}

		public Criteria andLoguserLessThan(String value) {
			addCriterion("LOGUSER <", value, "loguser");
			return (Criteria) this;
		}

		public Criteria andLoguserLessThanOrEqualTo(String value) {
			addCriterion("LOGUSER <=", value, "loguser");
			return (Criteria) this;
		}

		public Criteria andLoguserLike(String value) {
			addCriterion("LOGUSER like", value, "loguser");
			return (Criteria) this;
		}

		public Criteria andLoguserNotLike(String value) {
			addCriterion("LOGUSER not like", value, "loguser");
			return (Criteria) this;
		}

		public Criteria andLoguserIn(List<String> values) {
			addCriterion("LOGUSER in", values, "loguser");
			return (Criteria) this;
		}

		public Criteria andLoguserNotIn(List<String> values) {
			addCriterion("LOGUSER not in", values, "loguser");
			return (Criteria) this;
		}

		public Criteria andLoguserBetween(String value1, String value2) {
			addCriterion("LOGUSER between", value1, value2, "loguser");
			return (Criteria) this;
		}

		public Criteria andLoguserNotBetween(String value1, String value2) {
			addCriterion("LOGUSER not between", value1, value2, "loguser");
			return (Criteria) this;
		}

		public Criteria andNcOkIsNull() {
			addCriterion("NC_OK is null");
			return (Criteria) this;
		}

		public Criteria andNcOkIsNotNull() {
			addCriterion("NC_OK is not null");
			return (Criteria) this;
		}

		public Criteria andNcOkEqualTo(String value) {
			addCriterion("NC_OK =", value, "ncOk");
			return (Criteria) this;
		}

		public Criteria andNcOkNotEqualTo(String value) {
			addCriterion("NC_OK <>", value, "ncOk");
			return (Criteria) this;
		}

		public Criteria andNcOkGreaterThan(String value) {
			addCriterion("NC_OK >", value, "ncOk");
			return (Criteria) this;
		}

		public Criteria andNcOkGreaterThanOrEqualTo(String value) {
			addCriterion("NC_OK >=", value, "ncOk");
			return (Criteria) this;
		}

		public Criteria andNcOkLessThan(String value) {
			addCriterion("NC_OK <", value, "ncOk");
			return (Criteria) this;
		}

		public Criteria andNcOkLessThanOrEqualTo(String value) {
			addCriterion("NC_OK <=", value, "ncOk");
			return (Criteria) this;
		}

		public Criteria andNcOkLike(String value) {
			addCriterion("NC_OK like", value, "ncOk");
			return (Criteria) this;
		}

		public Criteria andNcOkNotLike(String value) {
			addCriterion("NC_OK not like", value, "ncOk");
			return (Criteria) this;
		}

		public Criteria andNcOkIn(List<String> values) {
			addCriterion("NC_OK in", values, "ncOk");
			return (Criteria) this;
		}

		public Criteria andNcOkNotIn(List<String> values) {
			addCriterion("NC_OK not in", values, "ncOk");
			return (Criteria) this;
		}

		public Criteria andNcOkBetween(String value1, String value2) {
			addCriterion("NC_OK between", value1, value2, "ncOk");
			return (Criteria) this;
		}

		public Criteria andNcOkNotBetween(String value1, String value2) {
			addCriterion("NC_OK not between", value1, value2, "ncOk");
			return (Criteria) this;
		}

		public Criteria andValidaIsNull() {
			addCriterion("VALIDA is null");
			return (Criteria) this;
		}

		public Criteria andValidaIsNotNull() {
			addCriterion("VALIDA is not null");
			return (Criteria) this;
		}

		public Criteria andValidaEqualTo(String value) {
			addCriterion("VALIDA =", value, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaNotEqualTo(String value) {
			addCriterion("VALIDA <>", value, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaGreaterThan(String value) {
			addCriterion("VALIDA >", value, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaGreaterThanOrEqualTo(String value) {
			addCriterion("VALIDA >=", value, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaLessThan(String value) {
			addCriterion("VALIDA <", value, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaLessThanOrEqualTo(String value) {
			addCriterion("VALIDA <=", value, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaLike(String value) {
			addCriterion("VALIDA like", value, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaNotLike(String value) {
			addCriterion("VALIDA not like", value, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaIn(List<String> values) {
			addCriterion("VALIDA in", values, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaNotIn(List<String> values) {
			addCriterion("VALIDA not in", values, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaBetween(String value1, String value2) {
			addCriterion("VALIDA between", value1, value2, "valida");
			return (Criteria) this;
		}

		public Criteria andValidaNotBetween(String value1, String value2) {
			addCriterion("VALIDA not between", value1, value2, "valida");
			return (Criteria) this;
		}

		public Criteria andAppNameIsNull() {
			addCriterion("APP_NAME is null");
			return (Criteria) this;
		}

		public Criteria andAppNameIsNotNull() {
			addCriterion("APP_NAME is not null");
			return (Criteria) this;
		}

		public Criteria andAppNameEqualTo(String value) {
			addCriterion("APP_NAME =", value, "appName");
			return (Criteria) this;
		}

		public Criteria andAppNameNotEqualTo(String value) {
			addCriterion("APP_NAME <>", value, "appName");
			return (Criteria) this;
		}

		public Criteria andAppNameGreaterThan(String value) {
			addCriterion("APP_NAME >", value, "appName");
			return (Criteria) this;
		}

		public Criteria andAppNameGreaterThanOrEqualTo(String value) {
			addCriterion("APP_NAME >=", value, "appName");
			return (Criteria) this;
		}

		public Criteria andAppNameLessThan(String value) {
			addCriterion("APP_NAME <", value, "appName");
			return (Criteria) this;
		}

		public Criteria andAppNameLessThanOrEqualTo(String value) {
			addCriterion("APP_NAME <=", value, "appName");
			return (Criteria) this;
		}

		public Criteria andAppNameLike(String value) {
			addCriterion("APP_NAME like", value, "appName");
			return (Criteria) this;
		}

		public Criteria andAppNameNotLike(String value) {
			addCriterion("APP_NAME not like", value, "appName");
			return (Criteria) this;
		}

		public Criteria andAppNameIn(List<String> values) {
			addCriterion("APP_NAME in", values, "appName");
			return (Criteria) this;
		}

		public Criteria andAppNameNotIn(List<String> values) {
			addCriterion("APP_NAME not in", values, "appName");
			return (Criteria) this;
		}

		public Criteria andAppNameBetween(String value1, String value2) {
			addCriterion("APP_NAME between", value1, value2, "appName");
			return (Criteria) this;
		}

		public Criteria andAppNameNotBetween(String value1, String value2) {
			addCriterion("APP_NAME not between", value1, value2, "appName");
			return (Criteria) this;
		}

		public Criteria andExIdIsNull() {
			addCriterion("EX_ID is null");
			return (Criteria) this;
		}

		public Criteria andExIdIsNotNull() {
			addCriterion("EX_ID is not null");
			return (Criteria) this;
		}

		public Criteria andExIdEqualTo(Long value) {
			addCriterion("EX_ID =", value, "exId");
			return (Criteria) this;
		}

		public Criteria andExIdNotEqualTo(Long value) {
			addCriterion("EX_ID <>", value, "exId");
			return (Criteria) this;
		}

		public Criteria andExIdGreaterThan(Long value) {
			addCriterion("EX_ID >", value, "exId");
			return (Criteria) this;
		}

		public Criteria andExIdGreaterThanOrEqualTo(Long value) {
			addCriterion("EX_ID >=", value, "exId");
			return (Criteria) this;
		}

		public Criteria andExIdLessThan(Long value) {
			addCriterion("EX_ID <", value, "exId");
			return (Criteria) this;
		}

		public Criteria andExIdLessThanOrEqualTo(Long value) {
			addCriterion("EX_ID <=", value, "exId");
			return (Criteria) this;
		}

		public Criteria andExIdIn(List<Long> values) {
			addCriterion("EX_ID in", values, "exId");
			return (Criteria) this;
		}

		public Criteria andExIdNotIn(List<Long> values) {
			addCriterion("EX_ID not in", values, "exId");
			return (Criteria) this;
		}

		public Criteria andExIdBetween(Long value1, Long value2) {
			addCriterion("EX_ID between", value1, value2, "exId");
			return (Criteria) this;
		}

		public Criteria andExIdNotBetween(Long value1, Long value2) {
			addCriterion("EX_ID not between", value1, value2, "exId");
			return (Criteria) this;
		}

		public Criteria andDeviceIsNull() {
			addCriterion("DEVICE is null");
			return (Criteria) this;
		}

		public Criteria andDeviceIsNotNull() {
			addCriterion("DEVICE is not null");
			return (Criteria) this;
		}

		public Criteria andDeviceEqualTo(String value) {
			addCriterion("DEVICE =", value, "device");
			return (Criteria) this;
		}

		public Criteria andDeviceNotEqualTo(String value) {
			addCriterion("DEVICE <>", value, "device");
			return (Criteria) this;
		}

		public Criteria andDeviceGreaterThan(String value) {
			addCriterion("DEVICE >", value, "device");
			return (Criteria) this;
		}

		public Criteria andDeviceGreaterThanOrEqualTo(String value) {
			addCriterion("DEVICE >=", value, "device");
			return (Criteria) this;
		}

		public Criteria andDeviceLessThan(String value) {
			addCriterion("DEVICE <", value, "device");
			return (Criteria) this;
		}

		public Criteria andDeviceLessThanOrEqualTo(String value) {
			addCriterion("DEVICE <=", value, "device");
			return (Criteria) this;
		}

		public Criteria andDeviceLike(String value) {
			addCriterion("DEVICE like", value, "device");
			return (Criteria) this;
		}

		public Criteria andDeviceNotLike(String value) {
			addCriterion("DEVICE not like", value, "device");
			return (Criteria) this;
		}

		public Criteria andDeviceIn(List<String> values) {
			addCriterion("DEVICE in", values, "device");
			return (Criteria) this;
		}

		public Criteria andDeviceNotIn(List<String> values) {
			addCriterion("DEVICE not in", values, "device");
			return (Criteria) this;
		}

		public Criteria andDeviceBetween(String value1, String value2) {
			addCriterion("DEVICE between", value1, value2, "device");
			return (Criteria) this;
		}

		public Criteria andDeviceNotBetween(String value1, String value2) {
			addCriterion("DEVICE not between", value1, value2, "device");
			return (Criteria) this;
		}

		public Criteria andCodComuneIsNull() {
			addCriterion("COD_COMUNE is null");
			return (Criteria) this;
		}

		public Criteria andCodComuneIsNotNull() {
			addCriterion("COD_COMUNE is not null");
			return (Criteria) this;
		}

		public Criteria andCodComuneEqualTo(Integer value) {
			addCriterion("COD_COMUNE =", value, "codComune");
			return (Criteria) this;
		}

		public Criteria andCodComuneNotEqualTo(Integer value) {
			addCriterion("COD_COMUNE <>", value, "codComune");
			return (Criteria) this;
		}

		public Criteria andCodComuneGreaterThan(Integer value) {
			addCriterion("COD_COMUNE >", value, "codComune");
			return (Criteria) this;
		}

		public Criteria andCodComuneGreaterThanOrEqualTo(Integer value) {
			addCriterion("COD_COMUNE >=", value, "codComune");
			return (Criteria) this;
		}

		public Criteria andCodComuneLessThan(Integer value) {
			addCriterion("COD_COMUNE <", value, "codComune");
			return (Criteria) this;
		}

		public Criteria andCodComuneLessThanOrEqualTo(Integer value) {
			addCriterion("COD_COMUNE <=", value, "codComune");
			return (Criteria) this;
		}

		public Criteria andCodComuneIn(List<Integer> values) {
			addCriterion("COD_COMUNE in", values, "codComune");
			return (Criteria) this;
		}

		public Criteria andCodComuneNotIn(List<Integer> values) {
			addCriterion("COD_COMUNE not in", values, "codComune");
			return (Criteria) this;
		}

		public Criteria andCodComuneBetween(Integer value1, Integer value2) {
			addCriterion("COD_COMUNE between", value1, value2, "codComune");
			return (Criteria) this;
		}

		public Criteria andCodComuneNotBetween(Integer value1, Integer value2) {
			addCriterion("COD_COMUNE not between", value1, value2, "codComune");
			return (Criteria) this;
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table VFIX_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public static class Criterion {
		private String condition;
		private Object value;
		private Object secondValue;
		private boolean noValue;
		private boolean singleValue;
		private boolean betweenValue;
		private boolean listValue;
		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue,
				String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}

	/**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table VFIX_SEGNALAZIONE
     *
     * @mbggenerated do_not_delete_during_merge Mon Apr 23 16:24:25 CEST 2018
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }
}