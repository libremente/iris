package it.venis.iris2.oggettidbiris;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.DELETE_FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.INSERT_INTO;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT_DISTINCT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.VALUES;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;

import it.venis.iris2.oggettidbiris.VfixFotografie;
import it.venis.iris2.oggettidbiris.VfixFotografieExample.Criteria;
import it.venis.iris2.oggettidbiris.VfixFotografieExample.Criterion;
import it.venis.iris2.oggettidbiris.VfixFotografieExample;
import java.util.List;
import java.util.Map;

public class VfixFotografieSqlProvider {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_FOTOGRAFIE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String countByExample(VfixFotografieExample example) {
		BEGIN();
		SELECT("count(*)");
		FROM("VFIX_FOTOGRAFIE");
		applyWhere(example, false);
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_FOTOGRAFIE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String deleteByExample(VfixFotografieExample example) {
		BEGIN();
		DELETE_FROM("VFIX_FOTOGRAFIE");
		applyWhere(example, false);
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_FOTOGRAFIE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String insertSelective(VfixFotografie record) {
		BEGIN();
		INSERT_INTO("VFIX_FOTOGRAFIE");
		if (record.getIdFotografia() != null) {
			VALUES("ID_FOTOGRAFIA", "#{idFotografia,jdbcType=DECIMAL}");
		}
		if (record.getPtrSegnalazione() != null) {
			VALUES("PTR_SEGNALAZIONE", "#{ptrSegnalazione,jdbcType=DECIMAL}");
		}
		if (record.getNomeFile() != null) {
			VALUES("NOME_FILE", "#{nomeFile,jdbcType=VARCHAR}");
		}
		if (record.getNomeFileThumb() != null) {
			VALUES("NOME_FILE_THUMB", "#{nomeFileThumb,jdbcType=VARCHAR}");
		}
		if (record.getInterno() != null) {
			VALUES("INTERNO", "#{interno,jdbcType=CHAR}");
		}
		if (record.getPtrUtente() != null) {
			VALUES("PTR_UTENTE", "#{ptrUtente,jdbcType=VARCHAR}");
		}
		if (record.getNomeFileThumbMid() != null) {
			VALUES("NOME_FILE_THUMB_MID",
					"#{nomeFileThumbMid,jdbcType=VARCHAR}");
		}
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_FOTOGRAFIE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String selectByExample(VfixFotografieExample example) {
		BEGIN();
		if (example != null && example.isDistinct()) {
			SELECT_DISTINCT("ID_FOTOGRAFIA");
		} else {
			SELECT("ID_FOTOGRAFIA");
		}
		SELECT("PTR_SEGNALAZIONE");
		SELECT("NOME_FILE");
		SELECT("NOME_FILE_THUMB");
		SELECT("INTERNO");
		SELECT("PTR_UTENTE");
		SELECT("NOME_FILE_THUMB_MID");
		FROM("VFIX_FOTOGRAFIE");
		applyWhere(example, false);
		if (example != null && example.getOrderByClause() != null) {
			ORDER_BY(example.getOrderByClause());
		}
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_FOTOGRAFIE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String updateByExampleSelective(Map<String, Object> parameter) {
		VfixFotografie record = (VfixFotografie) parameter.get("record");
		VfixFotografieExample example = (VfixFotografieExample) parameter
				.get("example");
		BEGIN();
		UPDATE("VFIX_FOTOGRAFIE");
		if (record.getIdFotografia() != null) {
			SET("ID_FOTOGRAFIA = #{record.idFotografia,jdbcType=DECIMAL}");
		}
		if (record.getPtrSegnalazione() != null) {
			SET("PTR_SEGNALAZIONE = #{record.ptrSegnalazione,jdbcType=DECIMAL}");
		}
		if (record.getNomeFile() != null) {
			SET("NOME_FILE = #{record.nomeFile,jdbcType=VARCHAR}");
		}
		if (record.getNomeFileThumb() != null) {
			SET("NOME_FILE_THUMB = #{record.nomeFileThumb,jdbcType=VARCHAR}");
		}
		if (record.getInterno() != null) {
			SET("INTERNO = #{record.interno,jdbcType=CHAR}");
		}
		if (record.getPtrUtente() != null) {
			SET("PTR_UTENTE = #{record.ptrUtente,jdbcType=VARCHAR}");
		}
		if (record.getNomeFileThumbMid() != null) {
			SET("NOME_FILE_THUMB_MID = #{record.nomeFileThumbMid,jdbcType=VARCHAR}");
		}
		applyWhere(example, true);
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_FOTOGRAFIE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String updateByExample(Map<String, Object> parameter) {
		BEGIN();
		UPDATE("VFIX_FOTOGRAFIE");
		SET("ID_FOTOGRAFIA = #{record.idFotografia,jdbcType=DECIMAL}");
		SET("PTR_SEGNALAZIONE = #{record.ptrSegnalazione,jdbcType=DECIMAL}");
		SET("NOME_FILE = #{record.nomeFile,jdbcType=VARCHAR}");
		SET("NOME_FILE_THUMB = #{record.nomeFileThumb,jdbcType=VARCHAR}");
		SET("INTERNO = #{record.interno,jdbcType=CHAR}");
		SET("PTR_UTENTE = #{record.ptrUtente,jdbcType=VARCHAR}");
		SET("NOME_FILE_THUMB_MID = #{record.nomeFileThumbMid,jdbcType=VARCHAR}");
		VfixFotografieExample example = (VfixFotografieExample) parameter
				.get("example");
		applyWhere(example, true);
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_FOTOGRAFIE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String updateByPrimaryKeySelective(VfixFotografie record) {
		BEGIN();
		UPDATE("VFIX_FOTOGRAFIE");
		if (record.getPtrSegnalazione() != null) {
			SET("PTR_SEGNALAZIONE = #{ptrSegnalazione,jdbcType=DECIMAL}");
		}
		if (record.getNomeFile() != null) {
			SET("NOME_FILE = #{nomeFile,jdbcType=VARCHAR}");
		}
		if (record.getNomeFileThumb() != null) {
			SET("NOME_FILE_THUMB = #{nomeFileThumb,jdbcType=VARCHAR}");
		}
		if (record.getInterno() != null) {
			SET("INTERNO = #{interno,jdbcType=CHAR}");
		}
		if (record.getPtrUtente() != null) {
			SET("PTR_UTENTE = #{ptrUtente,jdbcType=VARCHAR}");
		}
		if (record.getNomeFileThumbMid() != null) {
			SET("NOME_FILE_THUMB_MID = #{nomeFileThumbMid,jdbcType=VARCHAR}");
		}
		WHERE("ID_FOTOGRAFIA = #{idFotografia,jdbcType=DECIMAL}");
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table VFIX_FOTOGRAFIE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	protected void applyWhere(VfixFotografieExample example,
			boolean includeExamplePhrase) {
		if (example == null) {
			return;
		}
		String parmPhrase1;
		String parmPhrase1_th;
		String parmPhrase2;
		String parmPhrase2_th;
		String parmPhrase3;
		String parmPhrase3_th;
		if (includeExamplePhrase) {
			parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
			parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
			parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
			parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
			parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
			parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
		} else {
			parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
			parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
			parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
			parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
			parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
			parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
		}
		StringBuilder sb = new StringBuilder();
		List<Criteria> oredCriteria = example.getOredCriteria();
		boolean firstCriteria = true;
		for (int i = 0; i < oredCriteria.size(); i++) {
			Criteria criteria = oredCriteria.get(i);
			if (criteria.isValid()) {
				if (firstCriteria) {
					firstCriteria = false;
				} else {
					sb.append(" or ");
				}
				sb.append('(');
				List<Criterion> criterions = criteria.getAllCriteria();
				boolean firstCriterion = true;
				for (int j = 0; j < criterions.size(); j++) {
					Criterion criterion = criterions.get(j);
					if (firstCriterion) {
						firstCriterion = false;
					} else {
						sb.append(" and ");
					}
					if (criterion.isNoValue()) {
						sb.append(criterion.getCondition());
					} else if (criterion.isSingleValue()) {
						if (criterion.getTypeHandler() == null) {
							sb.append(String.format(parmPhrase1,
									criterion.getCondition(), i, j));
						} else {
							sb.append(String.format(parmPhrase1_th,
									criterion.getCondition(), i, j,
									criterion.getTypeHandler()));
						}
					} else if (criterion.isBetweenValue()) {
						if (criterion.getTypeHandler() == null) {
							sb.append(String.format(parmPhrase2,
									criterion.getCondition(), i, j, i, j));
						} else {
							sb.append(String.format(parmPhrase2_th,
									criterion.getCondition(), i, j,
									criterion.getTypeHandler(), i, j,
									criterion.getTypeHandler()));
						}
					} else if (criterion.isListValue()) {
						sb.append(criterion.getCondition());
						sb.append(" (");
						List<?> listItems = (List<?>) criterion.getValue();
						boolean comma = false;
						for (int k = 0; k < listItems.size(); k++) {
							if (comma) {
								sb.append(", ");
							} else {
								comma = true;
							}
							if (criterion.getTypeHandler() == null) {
								sb.append(String.format(parmPhrase3, i, j, k));
							} else {
								sb.append(String.format(parmPhrase3_th, i, j,
										k, criterion.getTypeHandler()));
							}
						}
						sb.append(')');
					}
				}
				sb.append(')');
			}
		}
		if (sb.length() > 0) {
			WHERE(sb.toString());
		}
	}
}