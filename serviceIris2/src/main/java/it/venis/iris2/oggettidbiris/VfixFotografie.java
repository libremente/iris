package it.venis.iris2.oggettidbiris;

public class VfixFotografie {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_FOTOGRAFIE.ID_FOTOGRAFIA
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private Long idFotografia;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_FOTOGRAFIE.PTR_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private Long ptrSegnalazione;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_FOTOGRAFIE.NOME_FILE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private String nomeFile;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_FOTOGRAFIE.NOME_FILE_THUMB
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private String nomeFileThumb;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_FOTOGRAFIE.INTERNO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private String interno;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_FOTOGRAFIE.PTR_UTENTE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private String ptrUtente;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_FOTOGRAFIE.NOME_FILE_THUMB_MID
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private String nomeFileThumbMid;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_FOTOGRAFIE.ID_FOTOGRAFIA
	 * @return  the value of VFIX_FOTOGRAFIE.ID_FOTOGRAFIA
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public Long getIdFotografia() {
		return idFotografia;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_FOTOGRAFIE.ID_FOTOGRAFIA
	 * @param idFotografia  the value for VFIX_FOTOGRAFIE.ID_FOTOGRAFIA
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setIdFotografia(Long idFotografia) {
		this.idFotografia = idFotografia;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_FOTOGRAFIE.PTR_SEGNALAZIONE
	 * @return  the value of VFIX_FOTOGRAFIE.PTR_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public Long getPtrSegnalazione() {
		return ptrSegnalazione;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_FOTOGRAFIE.PTR_SEGNALAZIONE
	 * @param ptrSegnalazione  the value for VFIX_FOTOGRAFIE.PTR_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setPtrSegnalazione(Long ptrSegnalazione) {
		this.ptrSegnalazione = ptrSegnalazione;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_FOTOGRAFIE.NOME_FILE
	 * @return  the value of VFIX_FOTOGRAFIE.NOME_FILE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_FOTOGRAFIE.NOME_FILE
	 * @param nomeFile  the value for VFIX_FOTOGRAFIE.NOME_FILE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setNomeFile(String nomeFile) {
		this.nomeFile = nomeFile;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_FOTOGRAFIE.NOME_FILE_THUMB
	 * @return  the value of VFIX_FOTOGRAFIE.NOME_FILE_THUMB
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String getNomeFileThumb() {
		return nomeFileThumb;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_FOTOGRAFIE.NOME_FILE_THUMB
	 * @param nomeFileThumb  the value for VFIX_FOTOGRAFIE.NOME_FILE_THUMB
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setNomeFileThumb(String nomeFileThumb) {
		this.nomeFileThumb = nomeFileThumb;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_FOTOGRAFIE.INTERNO
	 * @return  the value of VFIX_FOTOGRAFIE.INTERNO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String getInterno() {
		return interno;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_FOTOGRAFIE.INTERNO
	 * @param interno  the value for VFIX_FOTOGRAFIE.INTERNO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setInterno(String interno) {
		this.interno = interno;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_FOTOGRAFIE.PTR_UTENTE
	 * @return  the value of VFIX_FOTOGRAFIE.PTR_UTENTE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String getPtrUtente() {
		return ptrUtente;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_FOTOGRAFIE.PTR_UTENTE
	 * @param ptrUtente  the value for VFIX_FOTOGRAFIE.PTR_UTENTE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setPtrUtente(String ptrUtente) {
		this.ptrUtente = ptrUtente;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_FOTOGRAFIE.NOME_FILE_THUMB_MID
	 * @return  the value of VFIX_FOTOGRAFIE.NOME_FILE_THUMB_MID
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String getNomeFileThumbMid() {
		return nomeFileThumbMid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_FOTOGRAFIE.NOME_FILE_THUMB_MID
	 * @param nomeFileThumbMid  the value for VFIX_FOTOGRAFIE.NOME_FILE_THUMB_MID
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setNomeFileThumbMid(String nomeFileThumbMid) {
		this.nomeFileThumbMid = nomeFileThumbMid;
	}
}