package it.venis.iris2.oggettidbiris;

import java.math.BigDecimal;
import java.util.Date;

public class VfixSegnalazione_SIT extends VfixSegnalazione {
	
	private BigDecimal latitudine;
	private BigDecimal longitudine;
	private String tipologia;
	private Integer ptrTipoStato;	
	private String stato;
	private Date dataUltimaOperazione;
	private String esito;
	private Integer ptrTipoStatoReale;	
	private String statoReale;	
	private Short ptrMunic;

	public String getEsito() {
		return esito;
	}

	public void setEsito(String esito) {
		this.esito = esito;
	}

	public Date getDataUltimaOperazione() {
		return dataUltimaOperazione;
	}

	public void setDataUltimaOperazione(Date dataUltimaOperazione) {
		this.dataUltimaOperazione = dataUltimaOperazione;
	}

	public BigDecimal getLatitudine() {
		return latitudine;
	}

	public void setLatitudine(BigDecimal latitudine) {
		this.latitudine = latitudine;
	}

	public BigDecimal getLongitudine() {
		return longitudine;
	}

	public void setLongitudine(BigDecimal longitudine) {
		this.longitudine = longitudine;
	}

	public String getTipologia() {
		return tipologia;
	}

	public void setTipologia(String tipologia) {
		this.tipologia = tipologia;
	}

	public String getStato() {
		return stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}
	
	public Integer getPtrTipoStato() {
		return ptrTipoStato;
	}

	public void setPtrTipoStato(Integer ptrTipoStato) {
		this.ptrTipoStato = ptrTipoStato;
	}
	
	public Integer getPtrTipoStatoReale() {
		return ptrTipoStatoReale;
	}

	public void setPtrTipoStatoReale(Integer ptrTipoStatoReale) {
		this.ptrTipoStatoReale = ptrTipoStatoReale;
	}

	public String getStatoReale() {
		return statoReale;
	}

	public void setStatoReale(String statoReale) {
		this.statoReale = statoReale;
	}
	
	public Short getPtrMunic() {
		return ptrMunic;
	}

	public void setPtrMunic(Short ptrMunic) {
		this.ptrMunic = ptrMunic;
	}

	
	public VfixSegnalazione_SIT() {
		// TODO Auto-generated constructor stub
	}

}
