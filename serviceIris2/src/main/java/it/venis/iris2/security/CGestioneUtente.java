package it.venis.iris2.security;

import it.venis.iris2.db.utils.CDbUtils;
import it.venis.iris2.oggettidbiris.VfixEmailUrpExample;
import it.venis.iris2.oggettidbiris.VfixLogMimuv;
import it.venis.iris2.oggettidbiris.VfixLogMimuvExample;
import it.venis.iris2.oggettidbiris.VfixLogMimuvMapper;
import it.venis.iris2.oggettidbiris.VfixUtenteRegistrato;

import java.security.AlgorithmParameters;
import java.security.Key;
import java.security.SecureRandom;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.engines.RijndaelEngine;
import org.bouncycastle.crypto.modes.CFBBlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.paddings.ZeroBytePadding;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class CGestioneUtente {
	
	public static byte fromDIME = 10;
	public static byte fromUNCREDITED = 20;
	
	private String sCognome = null;
	private String sNome = null;
	private String sToken = null;
	private String sBearer = null;
	private String sEMail = null;
	private String sTelefono = null;
	private String sCF = null;
	
	public String getsCognome() {
		return sCognome;
	}
	public String getsNome() {
		return sNome;
	}
	public String getsToken() {
		return sToken;
	}
	public String getsBearer() {
		return sBearer;
	}
	public String getsEMail() {
		return sEMail;
	}
	public String getsTelefono() {
		return sTelefono;
	}
	public String getsCF() {
		return sCF;
	}
	public void setsCF(String sCF) {
		this.sCF = sCF;
	}
	
	
	public CGestioneUtente() {
		// TODO Auto-generated constructor stub
	}
	
	public String getCurrentUser ()
	{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
		    String currentUserName = authentication.getName();
		    return currentUserName;
		}
		return null;
	}
	
	
	public String getCurrentUserIrisID ()
	{
		CDbUtils dbu = new CDbUtils();
		String sUserToken = getCurrentUser();
		List<VfixUtenteRegistrato> ut = dbu.selectUtenteRegistrato(sUserToken);
		if (ut != null && ut.size() == 1)
			return ut.get(0).getIdUtenteRegistrato().toString();
		return null;
	}
	
	public VfixUtenteRegistrato getCurrentUserIris (String sExtToken)
	{
		CDbUtils dbu = new CDbUtils();
		String sUserToken = sExtToken;
		if (sUserToken == null)	sUserToken = getCurrentUser();
		if (vCheckIfURP(sUserToken)) return null;   // 07.03.2019, per il caso urp
		List<VfixUtenteRegistrato> ut = dbu.selectUtenteRegistrato(sUserToken);
		if (ut != null && ut.size() == 1)
			return ut.get(0);
		return null;
	}


	public boolean bControllaAbilitUtente (Long theSegn)
	{
		CDbUtils dbu = new CDbUtils();
		String sUserToken = getCurrentUser();
		return dbu.bControllaAbilitUtente(theSegn, sUserToken) != null;
	}

	
    public String sBearerDecrypt (String sBearer)
    {
    	String sRis = decryptWithAesCBC (sBearer,"CHIAVE","CHIAVE");
		return sRis; 
    }
    
    public boolean sGetDatiBySpid (String theBearer)
    {
    	String sRis;
		try {
			sRis = decryptWithAesCBC (theBearer,"CHIAVE","CHIAVE");
			JsonObject jsonObj = (JsonObject) new JsonParser().parse(sRis);
			if (jsonObj.get("familyName") != null) sCognome = jsonObj.get("familyName").getAsString();
			if (jsonObj.get("name") != null) sNome = jsonObj.get("name").getAsString();
			if (jsonObj.get("fiscalNumber") != null) sToken = jsonObj.get("fiscalNumber").getAsString();   // anche per Spid uso il CF
			sBearer = theBearer;
			if (jsonObj.get("email") != null) sEMail = jsonObj.get("email").getAsString();
			if (jsonObj.get("mobilePhone") != null) sTelefono = jsonObj.get("mobilePhone").getAsString();
			if (jsonObj.get("fiscalNumber") != null) sCF = jsonObj.get("fiscalNumber").getAsString();

		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
    	
		return true; 
    }


    private static String decryptWithAesCBC(String encrypted, String key, String iv)
    {
        try {
            byte[] ciphertext = org.bouncycastle.util.encoders.Base64.decode(encrypted);
            PaddedBufferedBlockCipher aes = new PaddedBufferedBlockCipher(new CFBBlockCipher(new RijndaelEngine(128),8), new ZeroBytePadding());
            CipherParameters ivAndKey = new ParametersWithIV(new KeyParameter(key.getBytes()), iv.getBytes());
            aes.init(false, ivAndKey);
            return new String(cipherData(aes, ciphertext));
        } catch (InvalidCipherTextException e) {
            throw new RuntimeException(e);
        }
    }

    private static byte[] cipherData(PaddedBufferedBlockCipher cipher, byte[] data) throws InvalidCipherTextException
    {
        int minSize = cipher.getOutputSize(data.length);
        byte[] outBuf = new byte[minSize];
        int length1 = cipher.processBytes(data, 0, data.length, outBuf, 0);
        int length2 = cipher.doFinal(outBuf, length1);
        int actualLength = length1 + length2;
        byte[] cipherArray = new byte[actualLength];
        for (int x = 0; x < actualLength; x++) {
            cipherArray[x] = outBuf[x];
        }
        return cipherArray;
    }
    
    
    public boolean sGetDatiByDiMe (String theBearer)
    {
		// specifica autenticazione utilizzata dal Comune di Venezia, qui rimossa
		return false; 
    }
    
    
    // ------------------------------------- utente non accreditato --------------------------------------------- //
    //----------------------------------------------------------------------------------------------------------- //

    // prepara la string AES
    public String sSetDatiByUncredited (String theData)
    {
    	String sRis;
    	String sDati = null;
    	String sAux = null;
		try {
			// legge la chiave segreta...
			java.util.ResourceBundle mybundle=java.util.ResourceBundle.getBundle("application");
			String chiave = mybundle.getString("nocred.key");
			Key secretKey  = new SecretKeySpec(chiave.getBytes("UTF-8"), "AES");
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			byte [] bFinal = cipher.doFinal(theData.getBytes());
			byte[] bToken1 = org.bouncycastle.util.encoders.Base64.encode(bFinal);
			sDati = new String(bToken1);

		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    	
		return sDati; 
    }

    
    // prepara la string AES
    public boolean sGetDatiByUncredited (String theData)
    {
    	String sRis;
    	String sDati = null;
    	String sAux = null;
		try {
			// legge la chiave segreta...
			java.util.ResourceBundle mybundle=java.util.ResourceBundle.getBundle("application");
			String chiave = mybundle.getString("nocred.key");
			Key secretKey  = new SecretKeySpec(chiave.getBytes("UTF-8"), "AES");
			Cipher cipher = Cipher.getInstance("AES");
			byte[] b1 = org.bouncycastle.util.encoders.Base64.decode(theData.getBytes());
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			byte [] bF = cipher.doFinal(b1);
			sDati = new String(bF, "UTF-8");			
			if (!sDati.contains("cognome")) return false;
			
			JsonParser parser = new JsonParser();
			JsonObject jsonObj = (JsonObject) parser.parse(sDati);
			jsonObj = jsonObj.getAsJsonObject("datiUtente");
			
			sNome = jsonObj.get("nome").getAsString();		
			sCognome = jsonObj.get("cognome").getAsString();
			sEMail = jsonObj.get("email").getAsString();
			// come token
			sToken = sEMail;
			
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
    	
		return true; 
    }
    
    
    // costretto dalle modalità di encoding e dalla fretta
    private String vCorreggi (String sErrata)
    {
    	sErrata = sErrata.replace("\'", "\\'");
    	sErrata = sErrata.replace("Ã¨", "è");
    	sErrata = sErrata.replace("Ã©", "é");
    	sErrata = sErrata.replace("Ã²", "ò");
    	sErrata = sErrata.replace("Ã§", "ç");
    	sErrata = sErrata.replace("Ã¹", "ù");
    	sErrata = sErrata.replace("Ã", "à");
    	sErrata = sErrata.replace("à¬", "ì");
    	return sErrata;
    }

    
    // se l'email è quella di URP, essa sta facendo la segnalazione per conto di qualcuno e, in questo caso, il token non può essere l'email perché verrebbero tutte associate
    // al medesimo utente
    private boolean vCheckIfURP (String sEMail)
    {
		CDbUtils dbu = new CDbUtils();
    	return dbu.vCheckIfURP(sEMail);
    }
}
