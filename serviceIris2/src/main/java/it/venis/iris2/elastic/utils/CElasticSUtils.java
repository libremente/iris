package it.venis.iris2.elastic.utils;


import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.mail.MessagingException;

import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.transport.NoNodeAvailableException;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentParser;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder.Type;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryAction;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.mail.javamail.JavaMailSender;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class CElasticSUtils {

	private TransportClient clientElasticSearch = null;
	private IndexRequestBuilder theIndex = null;
	private CreateIndexRequestBuilder myidx = null;
	private String es_host ="";
	private String es_index = "";

	public TransportClient getClientElasticSearch() {
		return clientElasticSearch;
	}

	public void setClientElasticSearch(TransportClient clientElasticSearch) {
		this.clientElasticSearch = clientElasticSearch;
	}
	
	public IndexRequestBuilder getTheIndex() {
		return theIndex;
	}

	public void setTheIndex(IndexRequestBuilder theIndex) {
		this.theIndex = theIndex;
	}
	
	
	public CElasticSUtils() {
		// TODO Auto-generated constructor stub	
		java.util.ResourceBundle mybundle=java.util.ResourceBundle.getBundle("application");
		es_host = mybundle.getString("es_host");
		es_index = mybundle.getString("es_index");
		

		try {
			clientElasticSearch = new PreBuiltTransportClient(Settings.EMPTY).addTransportAddress(new TransportAddress(InetAddress.getByName(es_host), 9300));
			theIndex = clientElasticSearch.prepareIndex(es_index, "report");

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	protected void finalize() throws Throwable {
	     try {
	    	clientElasticSearch.close();
			//System.out.println ("Finito");    	 
	     } finally {
	         super.finalize();
	     }
	 }
	

	public boolean bResettaIndice (boolean bDelete)
	{
		try {
			XContentBuilder xcb = null;
			if (bDelete)
			{
				DeleteIndexRequest request = new DeleteIndexRequest(es_index);
				clientElasticSearch.admin().indices().delete(request);
			}
			bCreaIndice (es_index);		
		} 
		catch (NoNodeAvailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return true;
	}

	
	public IndexResponse bInserisciDati (String json)
	{
		return theIndex.setSource(json, XContentType.JSON).get();
	}

	
	// cancellazione di un valore...
	public long bCancellaDati (Long theId)
	{
		BulkByScrollResponse response = DeleteByQueryAction.INSTANCE.newRequestBuilder(this.clientElasticSearch)
			    .filter(QueryBuilders.matchQuery("ID_SEGNALAZIONE", theId.toString())) 
			    .source(es_index)                                  
			    .get();                                             
		return response.getDeleted();                   
	}

	
	// ricerca
	
//	public String bCerca_old (Long lIdSegnalazione, String sQuery, int wFrom, int wSize, String sSort, String sFiltro)
//	{
//		
//		java.util.ResourceBundle mybundle=java.util.ResourceBundle.getBundle("application");
//		String es_index = mybundle.getString("es_index");
//		String es_type = mybundle.getString("es_type");
//
//		
//		// Ci sono le MatchQuery e le MultiMatchQuery
//	
//		try {
//			
//			Long lIdSegn = 0L;
//			if (lIdSegnalazione != 0L)
//				lIdSegn = lIdSegnalazione;
//			else
//			{
//				try 
//				{
//				  lIdSegn = Long.parseLong(sQuery.trim());
//				} catch (NumberFormatException e) {}
//			}
//
//			SearchRequestBuilder srb1 = clientElasticSearch.prepareSearch().setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
//					.setIndices(es_index).setTypes(es_type).setFrom(wFrom).setSize(wSize);
//			//SearchRequestBuilder srb2 = clientElasticSearch.prepareSearch().setIndices(es_index).setTypes(es_type).setFrom(wFrom).setSize(wSize);
//			//SearchRequestBuilder srb3 = clientElasticSearch.prepareSearch().setIndices(es_index).setTypes(es_type).setFrom(wFrom).setSize(wSize);
//			
//			sQuery = "immondizi";
//
//			if (lIdSegn != 0L) 
//				srb1.setQuery(QueryBuilders.matchQuery("ID_SEGNALAZIONE", lIdSegn));
//			else
//			{
//				//if (!sQuery.isEmpty()) srb1.setQuery(QueryBuilders.multiMatchQuery(sQuery,  "INDICAZIONI", "OGGETTO"));
//				
//				String sQueryTot = "";
//				if (!sQuery.isEmpty())
//					//sQueryTot = "(INDICAZIONI : \"*" + sQuery + "*\" OR OGGETTO : \"*" + sQuery + "*\")";
//				sQueryTot = "(INDICAZIONI :  \"" + sQuery + "\" OR DESCRIZIONE : \"" + sQuery + "\")";
//				
//				if (sFiltro.trim().length() != 0)
//				{
//					String sFiltroTot = "";
//					JsonObject oFiltro = new JsonParser().parse(sFiltro).getAsJsonObject();
//					Set<Entry<String, JsonElement>> myl = oFiltro.entrySet();
//					byte bIdx = 0;
//					for(Map.Entry <String, JsonElement> singleItem : myl)
//					 {  
//						sFiltroTot += singleItem.getKey() + " : " + singleItem.getValue();
//						if (++bIdx < myl.size()) sFiltroTot += " AND ";
//					 }
//					
//					if (!sQueryTot.isEmpty()) sQueryTot += " AND " + sFiltroTot;
//					else sQueryTot = sFiltroTot;
//				}
//				
//				srb1.setQuery(QueryBuilders.queryStringQuery(sQueryTot));
//				//srb1.setQuery(QueryBuilders.multiMatchQuery(sQuery,  "INDICAZIONI", "OGGETTO"));
//				
//			}
//			
//			/*MultiSearchResponse response = clientElasticSearch.prepareMultiSearch()
//			        .add(srb1)
//			        .add(srb2)
//			        .add(srb3)
//			        .get();*/ 
//			
////			BoolQueryBuilder qb = QueryBuilders.boolQuery();
////			
////			qb.must(QueryBuilders.multiMatchQuery(sQuery,  "INDICAZIONI", "OGGETTO")).must(QueryBuilders.queryStringQuery(sQueryTot));
////			
////	        .must(termQuery("content", "test4"))                 
////	        .mustNot(termQuery("content", "test2"))              
////	        .should(termQuery("content", "test3"))               
////	        .filter(termQuery("content", "test5"));   
//			
//			
//			SearchResponse response = srb1.get();
//						
//			return response.toString();
//		} catch (NoNodeAvailableException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		return null;
//	}

	
	private boolean bAggiornaIndice (String sNomeIndice)
	{
		String sMapping = "{\n" +                                      
		        "  \"properties\": {\n" +
		        "    \"DT_OPER\": {\n" +
		        "      \"type\": \"date\",\n" +
		        "      \"format\": \"yyyy-MM-dd HH:mm:ss\"\n" +		        
		        "    },\n" +
		        "    \"DATA_SEGNALAZIONE\": {\n" +
		        "      \"type\": \"date\",\n" +
		        "      \"format\": \"yyyy-MM-dd HH:mm:ss\"\n" +		        
		        "    },\n" +
		        "    \"location\": {\n" +
		        "      \"type\": \"geo_point\"\n" +
		        "    },\n" +
		        "    \"ITER\": {\n" +
				"    \"type\": \"nested\",\n" +   
				"    \"properties\": {\n" +          				
				"    \"DATA_OPER\": {\n" +
				"      \"type\": \"date\",\n" +
				"      \"format\": \"yyyy-MM-dd HH:mm:ss\"\n" +		        
				"    }\n" +	
				"    }\n" +				
				"    },\n" +
		        "    \"COMMENTI\": {\n" +
				"    \"type\": \"nested\",\n" +   
				"    \"properties\": {\n" +          				
				"    \"DATA_COMMENTO\": {\n" +
				"      \"type\": \"date\",\n" +
				"      \"format\": \"yyyy-MM-dd HH:mm:ss\"\n" +		        
				"    }\n" +	
				"    }\n" +				
				"    }\n" +				
		        "  }\n" +				
		        "}";

		
		clientElasticSearch.admin().indices().preparePutMapping(sNomeIndice)   
		.setType("report")
		.setSource(sMapping, XContentType.JSON)
		.get();
		
		return true;
	}
	
	
	
	public boolean bCreaIndice (String sNomeIndice)
	{
		try {
			clientElasticSearch.admin().indices().prepareCreate(sNomeIndice).get();
		} 
		catch (NoNodeAvailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		bAggiornaIndice (sNomeIndice);	
		return true;
	}

	
	
	/// prova versione alternativa----
	public String bCerca (Long lIdSegnalazione, String sQuery, int wFrom, int wSize, String sSort, String sFiltro, String dtInizio, String dtFine, String sIdUtenteRegistrato)
	{
		
		java.util.ResourceBundle mybundle=java.util.ResourceBundle.getBundle("application");
		String es_index = mybundle.getString("es_index");
		String es_type = mybundle.getString("es_type");
		
		// Ci sono le MatchQuery e le MultiMatchQuery	
		try {
			
			Long lIdSegn = 0L;
			if (lIdSegnalazione != 0L)
				lIdSegn = lIdSegnalazione;
			else
			{
				try 
				{
				  lIdSegn = Long.parseLong(sQuery.trim());
				} catch (NumberFormatException e) {}
			}
			
			SearchRequestBuilder srb1 = clientElasticSearch.prepareSearch().setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
					.setIndices(es_index).setTypes(es_type).setFrom(wFrom).setSize(wSize);
			
			BoolQueryBuilder qb = QueryBuilders.boolQuery();				
			
			
			if (lIdSegn != 0L) 
			{
				qb.must(QueryBuilders.matchQuery("ID_SEGNALAZIONE", lIdSegn));
				//srb1.setQuery(QueryBuilders.matchQuery("ID_SEGNALAZIONE", lIdSegn));
			}
			else
			{
				if (!sQuery.isEmpty())
					qb.must(QueryBuilders.multiMatchQuery(sQuery,  "INDICAZIONI", "OGGETTO").type(Type.PHRASE_PREFIX));
				
				if (sFiltro.trim().length() != 0)
				{
					String sFiltroTot = "";
					JsonObject oFiltro = new JsonParser().parse(sFiltro).getAsJsonObject();
					Set<Entry<String, JsonElement>> myl = oFiltro.entrySet();
					byte bIdx = 0;
					for(Map.Entry <String, JsonElement> singleItem : myl)
					 {  
						sFiltroTot += singleItem.getKey() + " : " + singleItem.getValue();
						if (++bIdx < myl.size()) sFiltroTot += " AND ";
					 }
			
					qb.must(QueryBuilders.queryStringQuery(sFiltroTot));
				}
				
				
				if (!dtInizio.isEmpty() || !dtFine.isEmpty())
				{
					RangeQueryBuilder rqb = QueryBuilders.rangeQuery("DATA_SEGNALAZIONE");
					if (!dtInizio.isEmpty()) rqb.gte (dtInizio += " 00:00:00");
					if (!dtFine.isEmpty()) rqb.lte (dtFine += " 23:59:59");
					qb.must (rqb);
				}
				
				qb.minimumShouldMatch();								
			}			
			
			if (sIdUtenteRegistrato != null) qb.must(QueryBuilders.matchQuery("PTR_UTENTE", sIdUtenteRegistrato));  // per "le mie segnalazioni"
			
			srb1.setQuery(qb).addSort("ID_SEGNALAZIONE", SortOrder.DESC);

			
			SearchResponse response = srb1.get();
			return response.toString();
		} catch (NoNodeAvailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
}
